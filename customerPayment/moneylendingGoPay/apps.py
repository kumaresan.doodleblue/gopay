from django.apps import AppConfig


class MoneylendinggopayConfig(AppConfig):
    name = 'moneylendingGoPay'
