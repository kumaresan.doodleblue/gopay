# Generated by Django 2.1.7 on 2019-03-19 08:42

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(db_column='customer_id', primary_key=True, serialize=False)),
                ('fullname', models.CharField(blank=True, max_length=100, null=True)),
                ('email', models.EmailField(blank=True, max_length=254, null=True, unique=True)),
                ('phone', models.CharField(blank=True, max_length=50, null=True)),
                ('user', models.OneToOneField(db_column='auth_user_id', on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Loan',
            fields=[
                ('id', models.AutoField(db_column='loan_id', primary_key=True, serialize=False)),
                ('loan_amount', models.BigIntegerField()),
                ('customer', models.ForeignKey(db_column='customer_id', on_delete=django.db.models.deletion.DO_NOTHING, to='moneylendingGoPay.Customer')),
            ],
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(db_column='payment_id', primary_key=True, serialize=False)),
                ('payment_number', models.IntegerField()),
                ('loan', models.ForeignKey(db_column='loan_id', on_delete=django.db.models.deletion.DO_NOTHING, to='moneylendingGoPay.Loan')),
            ],
        ),
        migrations.CreateModel(
            name='StatusLookup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('loan_status', models.CharField(blank=True, max_length=16, null=True, unique=True)),
            ],
        ),
        migrations.AddField(
            model_name='payment',
            name='payment_status',
            field=models.ForeignKey(db_column='payment_status_code', on_delete=django.db.models.deletion.DO_NOTHING, to='moneylendingGoPay.StatusLookup'),
        ),
        migrations.AddField(
            model_name='loan',
            name='loan_status',
            field=models.ForeignKey(db_column='loan_status_code', on_delete=django.db.models.deletion.DO_NOTHING, to='moneylendingGoPay.StatusLookup'),
        ),
    ]
