from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.gopayPayment.index, name='index'),
    url(r'^display', views.gopayPayment.displayData, name='display'),
    url(r'^insertUser', views.gopayPayment.insertUser, name='insert'),
    url(r'^initiatePayment', views.gopayPayment.initiatePayment, name='initiatePayment'),
    url(r'^cancelPayment', views.gopayPayment.cancelPayment, name='cancelpayment'),
    url(r'^transferSuccess', views.gopayPayment.transferSuccess, name='transfersuccess'),
    url(r'^transferFailure', views.gopayPayment.transferFailure, name='transferFailure'),
    url(r'^installment', views.gopayPayment.installment, name = 'installment'),

]


