from django.shortcuts import render
from django.http import HttpResponse
from moneylendingGoPay.models import Customer,StatusLookup,Loan,Payment,paymentdetailstable,gopayTransactionDetails
from django.contrib.auth.models import User
import time
from gopay import payments
from gopay.api import add_defaults
from rest_framework.decorators import api_view

from gopay.enums import PaymentInstrument, BankSwiftCode, Currency
from django.db.models import Sum

from django.db import connection
from django.core.serializers.json import DjangoJSONEncoder

import json



class gopayPayment:

    def index(request):
        user = User.objects.create_user(username='test1',
                                        email='test1@gmail.com',
                                        password='123456')
        return HttpResponse("success")

    def dictfetchall(cursor):
        "Return all rows from a cursor as a dict"
        columns = [col[0] for col in cursor.description]
        return ([
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ])


    def displayData(request):

        cursor = connection.cursor()

        cursor.execute('select * from moneylendingGoPay_gopayTransactionDetails')
        #a = cursor.fetchall()
        #col=[col[0] for col in cursor.description]
        a=gopayPayment.dictfetchall(cursor)

        #print(json.dumps())
        dd=json.dumps(
            a,
            sort_keys=True,
            indent=1,
            cls=DjangoJSONEncoder
        )


        return HttpResponse(dd)


    def insertUser(request):

        cu=Customer.objects.get(id=3)
        q=Loan(customer=cu, loan_status=StatusLookup.objects.get(id=1), loan_amount=300000)
        q.save()

        return HttpResponse("success")

    @api_view(['POST'])
    def initiatePayment(request):

        minimum_value = 15000

        goid = request.POST['goid']
        cliendId = request.POST['cliendId']
        clientSecret = request.POST['clientSecret']

        loanId=request.POST['id']
        loanAmount = Loan.objects.get(id=loanId).loan_amount
        paidAmount = paymentdetailstable.objects.filter(loan_id=loanId).aggregate(Sum('amount_paid'))
        print(paidAmount)

        if paidAmount['amount_paid__sum']==None:
            paidAmount=0
        else:
            paidAmount=paidAmount['amount_paid__sum']


        if (int(request.POST['amount']) < minimum_value ):

            status = "entered value is less"

        elif(paidAmount >= loanAmount ):
            status = "already closed"
        else:
            gopay = gopayPayment.given_client(goid,cliendId,clientSecret)

            payment = gopay.create_payment({
                'payer': {
                    'default_payment_instrument': PaymentInstrument.BANK_ACCOUNT,
                    'allowed_payment_instruments': [PaymentInstrument.BANK_ACCOUNT],
                    'default_swift': BankSwiftCode.FIO_BANKA,
                    'allowed_swifts': [BankSwiftCode.FIO_BANKA, BankSwiftCode.MBANK],
                    'contact': {
                        'first_name': 'Zbynek',
                        'last_name': 'Zak',
                        'email': 'test@test.cz',
                        'phone_number': '+420777456123',
                        'city': 'C.Budejovice',
                        'street': 'Plana 67',
                        'postal_code': '373 01',
                        'country_code': 'CZE',
                    },
                },
                'amount': request.POST['amount'],
                'currency': Currency.CZECH_CROWNS,
                'order_number': 'order-test - ' + time.strftime("%Y-%m-%d %H:%M:%S"),
                'order_description': 'python test',
                'callback': {
                    'return_url': 'http://www.your-url.tld/return',
                    'notification_url': 'http://www.your-url.tld/notify'
                }
            })

            id_payment = payment.json['id']
            status = gopay.get_status(id_payment)

            if (payment.json['state']=='CREATED'):

                paymentdetailsupdation = paymentdetailstable(loan_id=loanId, amount_paid=request.POST['amount'])

                paymentdetailsupdation.save()
                paidAmount = paymentdetailstable.objects.aggregate(Sum('amount_paid'))
                if (paidAmount['amount_paid__sum']>loanAmount):
                    print("closed")

                transactiondetailsGopay=gopayTransactionDetails(transaction_id=payment.json['id'], amount=payment.json['amount'],status_desc="CREATED", transaction_date=time.strftime("%Y-%m-%d %H:%M:%S"))
                transactiondetailsGopay.save()

        return HttpResponse(status)


    def cancelPayment(request):
        return HttpResponse("success")


    def transferSuccess(request):
        #q = Payment(loan=Loan.objects.get(id=1), payment_status=StatusLookup.objects.get(id=1), payment_number='123545')
        #q.save()
        return HttpResponse("success")


    def transferFailure(request):
        #q = Payment(loan= Loan.objects.get(id=1), payment_status= StatusLookup.objects.get(id=2), payment_number= '123545')
        #q.save()
        return HttpResponse("failed")

    def installment(request):

        id=request.GET['id']

        loanAmount = Loan.objects.get(id=id).loan_amount
        td=loanAmount/4
        d={}
        for i in range(4):
            d[i]=td
        k=[]
        k.append(d)
        return HttpResponse(k)


    def given_client(goid,cliendId,clientSecret,config=None):

        print(goid,cliendId,clientSecret)

        return payments(
            add_defaults(config, {
                'goid': goid,
                'clientId': cliendId,
                'clientSecret': clientSecret,
                'isProductionMode': False
            }),
            {
            }
        )



