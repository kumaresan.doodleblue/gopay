from django.db import models
from django.conf import settings
from django.core.validators import RegexValidator

# Create your models here.


class Customer(models.Model):
    id = models.AutoField(db_column='customer_id', primary_key=True)

    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
        db_column='auth_user_id')

    fullname = models.CharField(max_length=100, blank=True, null=True)
    email = models.EmailField(blank=True, null=True, unique=True)
    phone = models.CharField(max_length=50, blank=True, null=True)


class StatusLookup(models.Model):
    loan_status = models.CharField(max_length=16, unique=True, blank=True, null=True)


class Loan(models.Model):
    id = models.AutoField(db_column='loan_id', primary_key=True)
    customer = models.ForeignKey('Customer',
                                 models.DO_NOTHING,
                                 db_column='customer_id')
    loan_status = models.ForeignKey('StatusLookup',
                                    models.DO_NOTHING,
                                    db_column='loan_status_code')
    loan_amount = models.BigIntegerField()


class Payment(models.Model):
    id = models.AutoField(db_column='payment_id', primary_key=True)

    loan = models.ForeignKey(Loan, models.DO_NOTHING, db_column='loan_id')
    payment_status = models.ForeignKey(
        'StatusLookup', models.DO_NOTHING, db_column='payment_status_code')

    payment_number = models.IntegerField()


class paymentdetailstable(models.Model):

    id = models.AutoField(primary_key=True)
    loan_id=models.CharField(max_length=16)
    amount_paid=models.CharField(max_length=100)


class gopayTransactionDetails(models.Model):

    id = models.AutoField(primary_key=True)
    transaction_id= models.CharField(max_length=16)
    amount = models.BigIntegerField(blank=True, null=True)
    status_desc = models.CharField(max_length=60, blank=True, null=True)
    transaction_date = models.DateTimeField(blank=True, null=True)


"""
Loan(transaction_id=payment.json['id'], amount=payment.json['amount'], is_processed=,status_code=,status_desc=,transaction_date=,virtual_account=)
class FaspayTransaction(models.Model):

    id = models.AutoField(db_column='faspay_transaction_id', primary_key=True)
    transaction_id = models.CharField(max_length=16, unique=True, blank=True, null=True)
    amount = models.BigIntegerField(blank=True, null=True)
    is_processed = models.NullBooleanField(default=False)
    status_code = models.IntegerField(blank=True, null=True)
    status_desc = models.CharField(max_length=60, blank=True, null=True)
    transaction_date = models.DateTimeField(blank=True, null=True)
    virtual_account = models.CharField(
        max_length=50,
        blank=True,
        null=True,
        validators=[RegexValidator(
            regex='^[0-9]+$', message='Virtual account has to be numeric digits')
        ]
    )

    objects = FasPayTransactionManager()

    class Meta:
        db_table = 'faspay_transaction'
"""


