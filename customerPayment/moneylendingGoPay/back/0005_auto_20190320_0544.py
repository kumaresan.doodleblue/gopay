# Generated by Django 2.1.7 on 2019-03-20 05:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('moneylendingGoPay', '0004_auto_20190320_0542'),
    ]

    operations = [
        migrations.AlterField(
            model_name='loan',
            name='amount_paid',
            field=models.BigIntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='loan',
            name='balance_amount',
            field=models.BigIntegerField(null=True),
        ),
    ]
